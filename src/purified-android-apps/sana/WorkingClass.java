/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import net.dimjasevic.jpf.sana.service.ISanaService;
import net.dimjasevic.jpf.sana.service.SanaService;

import android.content.Intent;
import android.os.RemoteException;
import android.content.ComponentName;

public class WorkingClass {

    public static final int BIND_AUTO_CREATE = 0x0001;

    ISanaService sanaService;
    SanaConnection connection;

    class ClientDrone extends Thread {
	private String ID;
	public ClientDrone(String ID) {
	    this.ID = ID;
	}

	public void run() {
	    try {
		sanaService.startSimulator();

		sanaService.resetResults();
		sanaService.startScanner();

		sanaService.getStartTime();
		sanaService.getNumberOfSEUs();
		sanaService.getDataSize();
		sanaService.getStateId();

		sanaService.sendResults();
		
		sanaService.stopScanner();
		sanaService.stopSimulator();
	    } catch (RemoteException e) {
	    }
	}
    }

    public WorkingClass() {}

    public void execute() {

	sanaService = null;
	connection = new SanaConnection();
	Intent intent = new Intent();
	bindService(intent, connection, BIND_AUTO_CREATE);

	ClientDrone c1 = new ClientDrone("A");
	ClientDrone c2 = new ClientDrone("B");

	c1.start();
	c2.start();

	try {
	    c1.join();
	    c2.join();
	} catch (InterruptedException e) {
	}
    }

    void bindService(Intent service, SanaConnection conn, int flags) {
	conn.onServiceConnected(new ComponentName(), (new SanaService()).onBind(service));
    }

    class SanaConnection {
	
	public void onServiceConnected(ComponentName name, android.os.IBinder service) {
	    sanaService = ISanaService.Stub.asInterface(service);
	}

	public void onServiceDisconnected(ComponentName name) {
	    sanaService = null;
	}
    }
}
