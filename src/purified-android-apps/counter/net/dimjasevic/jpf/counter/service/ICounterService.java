/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: net/dimjasevic/jpf/counter/service/ICounterService.aidl
 */
package net.dimjasevic.jpf.counter.service;
public interface ICounterService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.dimjasevic.jpf.counter.service.ICounterService
{
private static final java.lang.String DESCRIPTOR = "net.dimjasevic.jpf.counter.service.ICounterService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.dimjasevic.jpf.counter.service.ICounterService interface.
 */
public static net.dimjasevic.jpf.counter.service.ICounterService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iiFace = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
return ((net.dimjasevic.jpf.counter.service.ICounterService)iiFace);
}
public android.os.IBinder asBinder()
{
return this;
}
}
public void reset() throws android.os.RemoteException;
public void increment() throws android.os.RemoteException;
public int status() throws android.os.RemoteException;
}
