/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import net.dimjasevic.jpf.counter.service.ICounterService;
import net.dimjasevic.jpf.counter.service.CounterService;

import android.content.Intent;
import android.os.RemoteException;
import android.content.ComponentName;

public class WorkingClass {

    public static final int BIND_AUTO_CREATE = 0x0001;

    ICounterService counterService;
    CounterConnection connection;

    class ClientDrone extends Thread {
	private String ID;
	public ClientDrone(String ID) {
	    this.ID = ID;
	}

	public void run() {
	    try {
		counterService.increment();
	    } catch (RemoteException e) {
	    }
	}
    }

    public WorkingClass() {}

    public void execute() {

	counterService = null;
	connection = new CounterConnection();
	Intent intent = new Intent();
	bindService(intent, connection, BIND_AUTO_CREATE);

	try {
	    counterService.reset();
	} catch (RemoteException e) {
	}

	ClientDrone c1 = new ClientDrone("A");
	ClientDrone c2 = new ClientDrone("B");

	c1.start();
	c2.start();

	try {
	    c1.join();
	    c2.join();
	} catch (InterruptedException e) {
	}
	
	try {
	    System.out.println(counterService.status());
	} catch (RemoteException e) {
	}
    }

    void bindService(Intent service, CounterConnection conn, int flags) {
	conn.onServiceConnected(new ComponentName(), (new CounterService()).onBind(service));
    }

    class CounterConnection {
	
	public void onServiceConnected(ComponentName name, android.os.IBinder service) {
	    counterService = ICounterService.Stub.asInterface(service);
	}

	public void onServiceDisconnected(ComponentName name) {
	    counterService = null;
	}
    }
}
