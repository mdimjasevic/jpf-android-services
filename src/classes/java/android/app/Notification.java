/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;

import android.content.Context;


public class Notification {
    public static final int FLAG_AUTO_CANCEL        = 0x00000010;

    public static final int FLAG_SHOW_LIGHTS        = 0x00000001;

    public int flags;

    public int ledARGB;

    public int ledOnMS;

    public int ledOffMS;

    public CharSequence tickerText;

    public Notification(int icon, CharSequence tickerText, long when) {
	this.tickerText = tickerText;
    }

    public void setLatestEventInfo(Context context,
            CharSequence contentTitle, CharSequence contentText, PendingIntent contentIntent) {
    }
}
