/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dimjasevic.jpf.sana.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

public class SanaService extends Service
{
    private boolean scannerStatus = false;
    private boolean simulatorStatus = false;

    private int resultsLock;

    @Override
    public IBinder onBind(Intent intent)
    {
	return new ISanaService.Stub() {

	    // data getters
	    public long getStartTime() throws RemoteException {
		return 42;
	    }

	    public int getNumberOfSEUs() throws RemoteException {
		return 7;
	    }

	    public int getDataSize() throws RemoteException {
		return 511;
	    }

	    public int getStateId() throws RemoteException {
		return 3;
	    }

	    // control
	    public void startScanner() throws RemoteException {
		if (!scannerStatus)
		    scannerStatus = true;
	    }

	    public void stopScanner() throws RemoteException {
		if (scannerStatus)
		    scannerStatus = false;
	    }

	    public void resetResults() throws RemoteException {
		resultsLock = 0;
	    }

	    public void sendResults() throws RemoteException {
		int t = resultsLock;
	    }


	    public void startSimulator() throws RemoteException {
		if (!simulatorStatus)
		    simulatorStatus = true;
	    }

	    public void stopSimulator() throws RemoteException {
		if (simulatorStatus)
		    simulatorStatus = false;
	    }
	};
    }
}
