/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dimjasevic.jpf.sana.service;

interface ISanaService {
    // data getters
    long getStartTime();     // in msec
    int getNumberOfSEUs();   // number of detected bit flips
    int getDataSize();       // in MB
    int getStateId();        // this returns a R.string.status_.. id suitable for retrieving the description

    // control
    void startScanner();
    void stopScanner();
    void resetResults();
    void sendResults();

    void startSimulator();
    void stopSimulator();
}
