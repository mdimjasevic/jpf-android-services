/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dimjasevic.jpf.sana.client;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import java.lang.Integer;
import java.lang.Long;

import net.dimjasevic.jpf.sana.service.ISanaService;

public class SanaClient extends Activity implements OnClickListener
{
    ISanaService sanaService;
    SanaConnection conn;
    private boolean scannerState = false;
    private boolean simulatorState = false;

    class SanaConnection implements ServiceConnection {
	
	public void onServiceConnected(ComponentName name, IBinder service) {
	    sanaService = ISanaService.Stub.asInterface(service);
	}

	public void onServiceDisconnected(ComponentName name) {
	    sanaService = null;
	}
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

	conn = new SanaConnection();
	Intent intent = new Intent("net.dimjasevic.jpf.sana.service.ISanaService");
	bindService(intent, conn, Context.BIND_AUTO_CREATE);

	((Button)findViewById(R.id.getStartTimeButton)).setOnClickListener(this);
	((Button)findViewById(R.id.getNumberOfSEUsButton)).setOnClickListener(this);
	((Button)findViewById(R.id.getDataSizeButton)).setOnClickListener(this);
	((Button)findViewById(R.id.getStateIdButton)).setOnClickListener(this);
	((Button)findViewById(R.id.toggleScannerStateButton)).setOnClickListener(this);
	((Button)findViewById(R.id.resetResultsButton)).setOnClickListener(this);
	((Button)findViewById(R.id.sendResultsButton)).setOnClickListener(this);
	((Button)findViewById(R.id.toggleSimulatorStateButton)).setOnClickListener(this);
    }

    public void onClick(View button) {
	try {
	    if (button.getId() == ((Button)findViewById(R.id.getStartTimeButton)).getId())
		((TextView)findViewById(R.id.sanaStatusTextView)).setText((new Long(sanaService.getStartTime())).toString());

	    else if (button.getId() == ((Button)findViewById(R.id.getNumberOfSEUsButton)).getId())
		((TextView)findViewById(R.id.sanaStatusTextView)).setText((new Integer(sanaService.getNumberOfSEUs())).toString());

	    else if (button.getId() == ((Button)findViewById(R.id.getDataSizeButton)).getId())
		((TextView)findViewById(R.id.sanaStatusTextView)).setText((new Integer(sanaService.getDataSize())).toString());

	    else if (button.getId() == ((Button)findViewById(R.id.getStateIdButton)).getId())
		((TextView)findViewById(R.id.sanaStatusTextView)).setText((new Integer(sanaService.getStateId())).toString());

	    else if (button.getId() == ((Button)findViewById(R.id.toggleScannerStateButton)).getId()) {
		if (scannerState) {
		    ((TextView)findViewById(R.id.toggleScannerStateButton)).setText("Start scanner");
		    ((TextView)findViewById(R.id.sanaStatusTextView)).setText("Scanner stopped");
		    sanaService.stopScanner();
		    scannerState = false;
		}
		else {
		    ((TextView)findViewById(R.id.toggleScannerStateButton)).setText("Stop scanner");
		    ((TextView)findViewById(R.id.sanaStatusTextView)).setText("Scanner started");
		    sanaService.startScanner();
		    scannerState = true;
		}
	    }

	    else if (button.getId() == ((Button)findViewById(R.id.resetResultsButton)).getId()) {
		sanaService.resetResults();
		((TextView)findViewById(R.id.sanaStatusTextView)).setText("Results reset");
	    }

	    else if (button.getId() == ((Button)findViewById(R.id.sendResultsButton)).getId()) {
		sanaService.sendResults();
		((TextView)findViewById(R.id.sanaStatusTextView)).setText("Results sent");
	    }

	    else if (button.getId() == ((Button)findViewById(R.id.toggleSimulatorStateButton)).getId()) {
		if (simulatorState) {
		    ((TextView)findViewById(R.id.toggleSimulatorStateButton)).setText("Start simulator");
		    ((TextView)findViewById(R.id.sanaStatusTextView)).setText("Simulator stopped");
		    sanaService.stopSimulator();
		    simulatorState = false;
		}
		else {
		    ((TextView)findViewById(R.id.toggleSimulatorStateButton)).setText("Stop simulator");
		    ((TextView)findViewById(R.id.sanaStatusTextView)).setText("Simulator started");
		    sanaService.startSimulator();
		    simulatorState = true;
		}
	    }
	} catch (RemoteException e) {
	    // Nothing in here
	}
    }

    @Override
    protected void onDestroy() {
	super.onDestroy();
	unbindService(conn);

	sanaService = null;
    }
}
