/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/marko/projects/gsoc/2012/jpf-android-services/android-apps/sana/service/src/net/dimjasevic/jpf/sana/service/ISanaService.aidl
 */
package net.dimjasevic.jpf.sana.service;
public interface ISanaService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.dimjasevic.jpf.sana.service.ISanaService
{
private static final java.lang.String DESCRIPTOR = "net.dimjasevic.jpf.sana.service.ISanaService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.dimjasevic.jpf.sana.service.ISanaService interface,
 * generating a proxy if needed.
 */
public static net.dimjasevic.jpf.sana.service.ISanaService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.dimjasevic.jpf.sana.service.ISanaService))) {
return ((net.dimjasevic.jpf.sana.service.ISanaService)iin);
}
return new net.dimjasevic.jpf.sana.service.ISanaService.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_getStartTime:
{
data.enforceInterface(DESCRIPTOR);
long _result = this.getStartTime();
reply.writeNoException();
reply.writeLong(_result);
return true;
}
case TRANSACTION_getNumberOfSEUs:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getNumberOfSEUs();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_getDataSize:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getDataSize();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_getStateId:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getStateId();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_startScanner:
{
data.enforceInterface(DESCRIPTOR);
this.startScanner();
reply.writeNoException();
return true;
}
case TRANSACTION_stopScanner:
{
data.enforceInterface(DESCRIPTOR);
this.stopScanner();
reply.writeNoException();
return true;
}
case TRANSACTION_resetResults:
{
data.enforceInterface(DESCRIPTOR);
this.resetResults();
reply.writeNoException();
return true;
}
case TRANSACTION_sendResults:
{
data.enforceInterface(DESCRIPTOR);
this.sendResults();
reply.writeNoException();
return true;
}
case TRANSACTION_startSimulator:
{
data.enforceInterface(DESCRIPTOR);
this.startSimulator();
reply.writeNoException();
return true;
}
case TRANSACTION_stopSimulator:
{
data.enforceInterface(DESCRIPTOR);
this.stopSimulator();
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.dimjasevic.jpf.sana.service.ISanaService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
// data getters

public long getStartTime() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
long _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getStartTime, _data, _reply, 0);
_reply.readException();
_result = _reply.readLong();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
// in msec

public int getNumberOfSEUs() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getNumberOfSEUs, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
// number of detected bit flips

public int getDataSize() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getDataSize, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
// in MB

public int getStateId() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getStateId, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
// this returns a R.string.status_.. id suitable for retrieving the description
// control

public void startScanner() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_startScanner, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void stopScanner() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_stopScanner, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void resetResults() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_resetResults, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void sendResults() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_sendResults, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void startSimulator() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_startSimulator, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void stopSimulator() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_stopSimulator, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_getStartTime = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_getNumberOfSEUs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_getDataSize = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_getStateId = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_startScanner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_stopScanner = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_resetResults = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_sendResults = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_startSimulator = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_stopSimulator = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
}
// data getters

public long getStartTime() throws android.os.RemoteException;
// in msec

public int getNumberOfSEUs() throws android.os.RemoteException;
// number of detected bit flips

public int getDataSize() throws android.os.RemoteException;
// in MB

public int getStateId() throws android.os.RemoteException;
// this returns a R.string.status_.. id suitable for retrieving the description
// control

public void startScanner() throws android.os.RemoteException;
public void stopScanner() throws android.os.RemoteException;
public void resetResults() throws android.os.RemoteException;
public void sendResults() throws android.os.RemoteException;
public void startSimulator() throws android.os.RemoteException;
public void stopSimulator() throws android.os.RemoteException;
}
