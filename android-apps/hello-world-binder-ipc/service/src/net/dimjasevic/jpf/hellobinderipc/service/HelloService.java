/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dimjasevic.jpf.hellobinderipc.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class HelloService extends Service
{
  @Override
  public IBinder onBind(Intent intent) {
    return new IHelloService.Stub() {

      public void log(String tag, String message) throws RemoteException {
        Log.d(tag, message);
      }
    };
  }
}
