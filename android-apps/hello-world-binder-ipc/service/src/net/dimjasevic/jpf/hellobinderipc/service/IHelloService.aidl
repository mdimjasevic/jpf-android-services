package net.dimjasevic.jpf.hellobinderipc.service;

interface IHelloService {
  void log(String tag, String message);
}
