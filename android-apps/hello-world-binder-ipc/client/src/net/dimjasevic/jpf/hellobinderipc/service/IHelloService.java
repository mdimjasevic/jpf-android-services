/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/marko/projects/gsoc/2012/jpf-android-services/android-apps/hello-world-binder-ipc/service/src/net/dimjasevic/jpf/hellobinderipc/service/IHelloService.aidl
 */
package net.dimjasevic.jpf.hellobinderipc.service;
public interface IHelloService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.dimjasevic.jpf.hellobinderipc.service.IHelloService
{
private static final java.lang.String DESCRIPTOR = "net.dimjasevic.jpf.hellobinderipc.service.IHelloService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.dimjasevic.jpf.hellobinderipc.service.IHelloService interface,
 * generating a proxy if needed.
 */
public static net.dimjasevic.jpf.hellobinderipc.service.IHelloService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.dimjasevic.jpf.hellobinderipc.service.IHelloService))) {
return ((net.dimjasevic.jpf.hellobinderipc.service.IHelloService)iin);
}
return new net.dimjasevic.jpf.hellobinderipc.service.IHelloService.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_log:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
this.log(_arg0, _arg1);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.dimjasevic.jpf.hellobinderipc.service.IHelloService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public void log(java.lang.String tag, java.lang.String message) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(tag);
_data.writeString(message);
mRemote.transact(Stub.TRANSACTION_log, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_log = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
}
public void log(java.lang.String tag, java.lang.String message) throws android.os.RemoteException;
}
