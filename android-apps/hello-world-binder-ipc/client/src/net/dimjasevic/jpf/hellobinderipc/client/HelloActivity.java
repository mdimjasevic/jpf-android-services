/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dimjasevic.jpf.hellobinderipc.client;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import net.dimjasevic.jpf.hellobinderipc.service.IHelloService;

public class HelloActivity extends Activity implements OnClickListener
{
    private static final String TAG = "HelloActivity";
    IHelloService helloService;
    HelloConnection conn;

    class HelloConnection implements ServiceConnection {
	
	public void onServiceConnected(ComponentName name, IBinder service) {
	    helloService = IHelloService.Stub.asInterface(service);
	    Log.i(TAG, "connected");
	}

	public void onServiceDisconnected(ComponentName name) {
	    helloService = null;
	    Log.i(TAG, "disconnected");
	}
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.main);

	conn = new HelloConnection();
	Intent intent = new Intent("net.dimjasevic.jpf.hellobinderipc.service.IHelloService");
	bindService(intent, conn, Context.BIND_AUTO_CREATE);

	((Button)findViewById(R.id.theButton)).setOnClickListener(this);
    }

    public void onClick(View button) {
	try {
	    helloService.log("HelloClient", "Hello from onClick()");
	} catch (RemoteException e) {
	    Log.e(TAG, "onClick failed", e);
	}
    }

    @Override
    protected void onDestroy() {
	super.onDestroy();
	Log.d(TAG, "onDestroyed");
	unbindService(conn);

	helloService = null;
    }
}
