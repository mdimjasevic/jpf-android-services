/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/marko/projects/gsoc/2012/jpf-android-services/android-apps/counter/service/src/net/dimjasevic/jpf/counter/service/ICounterService.aidl
 */
package net.dimjasevic.jpf.counter.service;
public interface ICounterService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements net.dimjasevic.jpf.counter.service.ICounterService
{
private static final java.lang.String DESCRIPTOR = "net.dimjasevic.jpf.counter.service.ICounterService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an net.dimjasevic.jpf.counter.service.ICounterService interface,
 * generating a proxy if needed.
 */
public static net.dimjasevic.jpf.counter.service.ICounterService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof net.dimjasevic.jpf.counter.service.ICounterService))) {
return ((net.dimjasevic.jpf.counter.service.ICounterService)iin);
}
return new net.dimjasevic.jpf.counter.service.ICounterService.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_reset:
{
data.enforceInterface(DESCRIPTOR);
this.reset();
reply.writeNoException();
return true;
}
case TRANSACTION_increment:
{
data.enforceInterface(DESCRIPTOR);
this.increment();
reply.writeNoException();
return true;
}
case TRANSACTION_status:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.status();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements net.dimjasevic.jpf.counter.service.ICounterService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public void reset() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_reset, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void increment() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_increment, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public int status() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_status, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_reset = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_increment = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_status = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
}
public void reset() throws android.os.RemoteException;
public void increment() throws android.os.RemoteException;
public int status() throws android.os.RemoteException;
}
