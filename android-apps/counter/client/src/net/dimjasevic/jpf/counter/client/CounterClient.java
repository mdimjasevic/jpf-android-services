/*
 * Copyright (C) 2012 Marko Dimjašević
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dimjasevic.jpf.counter.client;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import java.lang.Integer;

import net.dimjasevic.jpf.counter.service.ICounterService;

public class CounterClient extends Activity implements OnClickListener
{
    ICounterService counterService;
    CounterConnection conn;

    class CounterConnection implements ServiceConnection {
	
	public void onServiceConnected(ComponentName name, IBinder service) {
	    counterService = ICounterService.Stub.asInterface(service);
	}

	public void onServiceDisconnected(ComponentName name) {
	    counterService = null;
	}
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

	conn = new CounterConnection();
	Intent intent = new Intent("net.dimjasevic.jpf.counter.service.ICounterService");
	bindService(intent, conn, Context.BIND_AUTO_CREATE);

	((Button)findViewById(R.id.resetButton)).setOnClickListener(this);
	((Button)findViewById(R.id.incrementButton)).setOnClickListener(this);
    }

    public void onClick(View button) {
	try {
	    if (button.getId() == ((Button)findViewById(R.id.resetButton)).getId())
		counterService.reset();
	    else if (button.getId() == ((Button)findViewById(R.id.incrementButton)).getId())
		counterService.increment();

	    ((TextView)findViewById(R.id.counterStatusTextView)).setText((new Integer(counterService.status())).toString());

	} catch (RemoteException e) {
	    // Nothing in here
	}
    }

    @Override
    protected void onDestroy() {
	super.onDestroy();
	unbindService(conn);

	counterService = null;
    }
}
